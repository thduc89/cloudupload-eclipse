package vn.android.cloudupload;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.StorageObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class CloudPhotoManager {

	/**
	 * Object dùng để kết nối đến cloud api.
	 */
	private static Storage storage;

	/**
	 * Tên của bucket dùng để upload hình lên cloud.
	 */
    public static final String BUCKET_NAME = "cloud-upload";
    /**
     * Google Project number.
     */
    private static final String PROJECT_NUMBER = "93597047044";
	private static final String APPLICATION_NAME = "Cloud Upload";
    /**
     * Email address of service account in Project's Credentials page.
     */
	private static final String EMAIL_ADDRESS = "93597047044-vrds2tictbpk5f3ct1bo96lo9sia5l34@developer.gserviceaccount.com";
    /**
     * Path to private key file in assets folder.
     */
	private static final String PRIVATE_KEY_PATH = "private-key/Cloud Upload-c02385565ffe.p12";
	private static Context context;


	public static void init(Context context) {
		CloudPhotoManager.context = context;
	}

	/**
	 * Upload hình lên cloud.
	 * 
	 * @param bucketName
	 * @param filePath
	 * @param callback
	 */
	public static void uploadFile(final String bucketName,final String filePath,
                                  final ActionCallback callback){

		// Tạo 1 async task để chạy request trong background thread.
        AsyncTask<Void,Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                boolean uploadSuccess = false;
                try {
                	// Lấy storage.
                    Storage storage = getStorage();

                    StorageObject object = new StorageObject();
                    object.setBucket(bucketName);

                    File file = new File(filePath);

                    InputStream stream = new FileInputStream(file);
                    try {
                    	// Lấy type của file upload.
                        String contentType = URLConnection.guessContentTypeFromStream(stream);
                        InputStreamContent content = new InputStreamContent(contentType, stream);

                        // Tạo 1 insert object để thêm file vào bucket trên cloud.
                        Storage.Objects.Insert insert = storage.objects().insert(bucketName, null, content);
                        insert.setName(file.getName());

                        StorageObject obj = insert.execute();
                        Log.i("Uploaded photo", obj.toPrettyString());
                        uploadSuccess = true;
                    } finally {
                        stream.close();
                    }
                } catch(Exception exception){
                    exception.printStackTrace();
                }
                return uploadSuccess;
            }

            @Override
            protected void onPostExecute(Boolean uploadSuccess) {
                super.onPostExecute(uploadSuccess);
                if(callback != null){
                    if(uploadSuccess)
                        callback.onSuccess(true);
                    else
                        callback.onFail(false);

                }
            }
        };
        task.execute();
	}

	/**
	 * Tải hình trên cloud về máy.
	 * @param bucketName
	 * @param fileName
	 * @param callback
	 */
    public static void downloadFile(final String bucketName, final String fileName,
                                    final ActionCallback callback){

        AsyncTask<Void, Void, Boolean> task = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                boolean downloadSuccess = false;
                try{
                	// Tạo folder cloud-downloaded-photos để lưu hình download về trong bộ nhớ máy.
                    File directory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/cloud-downloaded-photos");
                    // Nếu folder chưa tồn tại thì tạo mới.
                    if(!directory.isDirectory()) {
                        directory.mkdirs();
                    }
                    // Tạo file để chứa hình download về.
                    File file = new File(directory.getAbsolutePath() + "/" + fileName);

                    Storage storage = getStorage();

                    //Tạo 1 get object để download hình.
                    Storage.Objects.Get get = storage.objects().get(bucketName, fileName);
                    FileOutputStream stream = new FileOutputStream(file);
                    try {
                        get.executeMediaAndDownloadTo(stream);
                        // Thêm hình vừa download vào gallery.
                        addPictureToGallery(file);
                        downloadSuccess = true;
                    } finally {
                        stream.close();
                    }
                }catch (Exception exception){
                    exception.printStackTrace();
                }
                return downloadSuccess;
            }

            @Override
            protected void onPostExecute(Boolean downloadSuccess) {
                super.onPostExecute(downloadSuccess);
                if(callback != null){
                    if(downloadSuccess)
                        callback.onSuccess(true);
                    else
                        callback.onFail(false);
                }
            }
        };
        task.execute();
    }

    /**
     * Thêm hình vào gallery từ 1 file trong bộ nhớ máy.
     * @param file
     */
    private static void addPictureToGallery(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    /**
     * Lấy danh sach bucket trên cloud.
     * @param callback
     */
    public static void getBuckets(final ActionCallback callback){

        AsyncTask<Void, Void, List<String>> task = new AsyncTask<Void, Void, List<String>>() {
            @Override
            protected List<String> doInBackground(Void... params) {
                List<String> list = null;
                try {
                    Storage storage = getStorage();

                    List<Bucket> buckets = storage.buckets().list(PROJECT_NUMBER).execute().getItems();
                    if (buckets != null) {
                        list = new ArrayList<>();
                        for (Bucket b : buckets) {
                            list.add(b.getName());
                        }
                    }
                }catch(Exception exception){
                    exception.printStackTrace();
                }

                return list;
            }

            @Override
            protected void onPostExecute(List<String> list) {
                super.onPostExecute(list);
                if(callback != null){
                    if(list != null)
                        callback.onSuccess(list);
                    else
                        callback.onFail(null);
                }
            }
        };
        task.execute();
    }

    /**
     * Lấy danh sách hình trong bucket trên cloud.
     * @param bucketName
     * @param callback
     */
    public static void getPhotosInBucket(final String bucketName,
                                                 final ActionCallback callback){
        AsyncTask<Void, Void, List<String>> task = new AsyncTask<Void, Void, List<String>>() {
            @Override
            protected List<String> doInBackground(Void... params) {
                List<String> list = null;
                try {
                    Storage storage = getStorage();

                    List<StorageObject> objects = storage.objects().list(bucketName).execute().getItems();
                    if (objects != null) {
                        list = new ArrayList<>();
                        for (StorageObject o : objects) {
                            list.add(o.getName());
                        }
                    }
                }catch(Exception exception){
                    exception.printStackTrace();
                }

                return list;
            }

            @Override
            protected void onPostExecute(List<String> list) {
                super.onPostExecute(list);
                if(callback != null){
                    if(list != null)
                        callback.onSuccess(list);
                    else
                        callback.onFail(null);
                }
            }
        };
        task.execute();
    }

    /**
     * Lấy object storage.
     * 
     * @return
     * @throws Exception
     */
	private static Storage getStorage() throws Exception {
		if (storage == null) {

			// Tạo http transport và json factory để sử dụng JSON API.
			HttpTransport httpTransport = new NetHttpTransport();
			JsonFactory jsonFactory = new JacksonFactory();

			// Phân quyền cho ứng dụng là full control.
			List<String> scopes = new ArrayList<>();
			scopes.add(StorageScopes.DEVSTORAGE_FULL_CONTROL);

			// Lấy file private key trong folder assets.
			AssetManager am = context.getAssets();
			InputStream inputStream = am.open(PRIVATE_KEY_PATH);
			File file = createFileFromInputStream(inputStream);

			// Khai báo thông tin chứng thực OAuth 2.0.
			Credential credential = new GoogleCredential.Builder()
					.setTransport(httpTransport).setJsonFactory(jsonFactory)
					.setServiceAccountId(EMAIL_ADDRESS)
					.setServiceAccountPrivateKeyFromP12File(file)
					.setServiceAccountScopes(scopes).build();

			// Tạo object storage từ các thông tin trên.
			storage = new Storage.Builder(httpTransport, jsonFactory,
					credential).setApplicationName(APPLICATION_NAME).build();
		}

		return storage;
	}

	/**
	 * Tạo file private key từ inputstream.
	 * @param inputStream
	 * @return
	 */
	private static File createFileFromInputStream(InputStream inputStream) {

		try {
			File f = new File(context.getFilesDir(), "cloud-private-key.p12");
			OutputStream outputStream = new FileOutputStream(f);
			byte buffer[] = new byte[1024];
			int length = 0;

			while ((length = inputStream.read(buffer)) > 0) {
				outputStream.write(buffer, 0, length);
			}

			outputStream.close();
			inputStream.close();

			return f;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
}
