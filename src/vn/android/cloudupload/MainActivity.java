package vn.android.cloudupload;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends Activity {

	/**
	 * Request code khi chuyển sang ứng dụng chọn ảnh trên máy.
	 */
	private static final int SELECT_PHOTO = 1000;
    private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		CloudPhotoManager.init(this);

        dialog = new ProgressDialog(this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
	}

    @Override
    protected void onResume() {
        super.onResume();
        // Kiểm tra nếu không có kết nối mạng internet thì hiện thông báo.
        if(!isNetworkAvailable(this)){
            AlertDialog.Builder b = new AlertDialog.Builder(this);
            b.setTitle("Không có kết nối mạng!");
            b.setMessage("Bạn có muốn mở kết nối mạng lên không?");
            b.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                	// Mở settings của máy lên.
                    startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
                }
            });
            b.setNegativeButton("Không", null);
            b.create().show();
        }
    }

    /**
     * Sự kiện nhấn vào nút Upload Photo.
     * @param view
     */
    public void uploadPhoto(View view) {
    	// Mở ứng dụng dùng để chọn hình trong máy lên.
		Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
		photoPickerIntent.setType("image/*");
		startActivityForResult(photoPickerIntent, SELECT_PHOTO);
	}

    /**
     * Sự kiện nhấn vào nút Download Photo.
     * @param view
     */
    public void downloadPhoto(View view){
        showProgressDialog("Xin chờ...");

        // Lấy danh sách bucket trên cloud.
        CloudPhotoManager.getBuckets(new ActionCallback() {
            @Override
            public void onSuccess(Object data) {
                dialog.cancel();

                final List<String> buckets = (List<String>) data;
                
                // Hiện danh sách bucket nhận được.
                ListAdapter adapter = new ArrayAdapter<>(MainActivity.this,
                        android.R.layout.simple_list_item_1,
                        buckets.toArray());

                AlertDialog.Builder bucketDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                bucketDialogBuilder.setTitle("Chọn thư mục");
                bucketDialogBuilder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface d, int which) {
                        final String bucketName = buckets.get(which);

                        showProgressDialog("Xin chờ...");

                        // Lấy danh sách hình đã upload trong bucket vừa chọn.
                        CloudPhotoManager.getPhotosInBucket
                                (bucketName, new ActionCallback() {
                                    @Override
                                    public void onSuccess(Object data) {
                                        dialog.cancel();

                                        final List<String> photos = (List<String>) data;
                                        
                                        // Hiện danh sách hình nhận được.
                                        ListAdapter photoAdapter = new ArrayAdapter<>(MainActivity.this,
                                                android.R.layout.simple_list_item_1, photos.toArray());

                                        AlertDialog.Builder photoDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                                        photoDialogBuilder.setTitle("Chọn hình");
                                        photoDialogBuilder.setAdapter(photoAdapter, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(final DialogInterface d, int which) {
                                                showProgressDialog("Đang tải...");

                                                // Download hình vừa chọn.
                                                CloudPhotoManager.downloadFile(bucketName, photos.get(which), new ActionCallback() {
                                                    @Override
                                                    public void onSuccess(Object data) {
                                                        dialog.cancel();
                                                        Toast.makeText(MainActivity.this,
                                                                "Tải hình thành công!",
                                                                Toast.LENGTH_SHORT).show();
                                                    }

                                                    @Override
                                                    public void onFail(Object data) {
                                                        dialog.cancel();
                                                        Toast.makeText(MainActivity.this,
                                                                "Tải hình không thành công! " +
                                                                        "Xin thử lại sau.",
                                                                Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            }
                                        });
                                        photoDialogBuilder.create().show();
                                    }

                                    @Override
                                    public void onFail(Object data) {
                                        dialog.cancel();
                                        Toast.makeText(MainActivity.this,
                                                "Lấy danh sách hình trong thư mục không thành " +
                                                        "công! Xin thử lại sau.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                });
                bucketDialogBuilder.create().show();
            }

            @Override
            public void onFail(Object data) {
                dialog.cancel();
                Toast.makeText(MainActivity.this, "Lấy danh sách bucket không thành công! Xin thử" +
                                " lại sau.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Sự kiện khi nhấn vào nút About.
     * @param view
     */
    public void showAbout(View view){
    	// Hiện activity About lên.
        startActivity(new Intent(this,AboutActivity.class));
    }

    /**
     * Nhận hình đã chọn từ ứng dụng chọn hình trong máy.
     */
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

		// Kiểm tra nếu requestCode là chọn hình và kết quả trả về là chọn thành công (RESULT_OK).
		if(requestCode == SELECT_PHOTO && resultCode == RESULT_OK){
			// Lấy URI của hình đã chọn.
			Uri selectedImage = imageReturnedIntent.getData();
			if (selectedImage != null) {
				// Lấy đường dẫn của hình đã chọn trong máy.
				String filePath = getRealPathFromURI(selectedImage);
				if (filePath != null)
                    showProgressDialog("Đang upload hình...");

					// Upload hình đã chọn lên cloud.
					CloudPhotoManager.uploadFile(CloudPhotoManager.BUCKET_NAME, filePath, new ActionCallback() {
                        @Override
                        public void onSuccess(Object data) {
                            dialog.cancel();
                            Toast.makeText(MainActivity.this, "Upload hình thành công!",
                                    Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFail(Object data) {
                            dialog.cancel();
                            Toast.makeText(MainActivity.this, "Upload hình không thành công! " +
                                            "Xin thử lại sau.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
			}
		}
	}

    private void showProgressDialog(String message){
        dialog.setMessage(message);
        dialog.show();
    }

    /**
     * Lấy đường dẫn của file hình trong máy dựa vào URI.
     * @param contentURI
     * @return
     */
	private String getRealPathFromURI(Uri contentURI) {
		Cursor cursor = getContentResolver().query(contentURI, null, null,
				null, null);
		if (cursor == null) {
			return contentURI.getPath();
		} else {
			cursor.moveToFirst();
			int idx = cursor
					.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			return cursor.getString(idx);
		}
	}

	/**
	 * Kiểm tra kết nối mạng internet.
	 * @param context
	 * @return
	 */
    private boolean isNetworkAvailable(Context context)
    {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

}
