package vn.android.cloudupload;

public interface ActionCallback {
	public void onSuccess(Object data);

	public void onFail(Object data);
}
